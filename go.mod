module gitlab.com/paritad/lersri_kubotra_v2

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/spf13/viper v1.12.0
	go.mongodb.org/mongo-driver v1.9.1
	google.golang.org/grpc v1.47.0
	google.golang.org/protobuf v1.28.1
)
