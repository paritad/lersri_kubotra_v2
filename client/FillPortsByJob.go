package main

import (
	"context"
	"fmt"

	pb "gitlab.com/paritad/lersri_kubotra_v2/proto/api/lersri_kubotra_api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DataRequest struct {
	JobID  string
	Output string
}

func SaveData(client pb.LersiDataServiceClient, _jobID string, _output string) (*pb.FillPortsByJobRes, error) {
	//ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	//defer cancel()
	lersidata := &pb.LersiData{
		Id:     "",
		Jobid:  _jobID,
		Output: _output,
	}

	lsi, err := client.FillPortsByJob(
		context.TODO(),
		&pb.FillPortsByJobReq{LersiData: lersidata},
	)

	if err != nil {
		return nil, err
	}

	statusCode := status.Code(err)
	if statusCode != codes.OK {
		return nil, err
	}

	return lsi, err
}

func SaveDBClient(jobID string, output string) {
	opts := []grpc.DialOption{grpc.WithInsecure()}
	conn, err := grpc.Dial("127.0.0.1:10000", opts...)
	if err != nil {
		panic(err)
	}

	client := pb.NewLersiDataServiceClient(conn)
	_, err = SaveData(client, jobID, output)
	if err != nil {
		//panic(err)
	}
	fmt.Println("Finish")
}

/// fortest
func main() {

	// input type
	// FillMoreInfoByJob ip_domain,port_name,moreinfo_output,moreinfo_tool
	SaveDBClient("8006d859-c127-4518-8a8b-d60e3e43e34a", string(`{"domain": "rocket.incognitolab.com","ip":"192.168.167.4", "port": "81"}`))
}
