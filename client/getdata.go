package main

import (
	"context"
	"fmt"
	"encoding/json"
	pb "gitlab.com/paritad/lersri_kubotra_v2/proto/api/lersri_kubotra_api"
	"google.golang.org/grpc"
)

type DataRequest struct {
	JobID      string
	Attributes string
	Output     string
}

func GetData(client pb.LersiDataServiceClient, jobID string) string {
	req := &pb.ReadLersiDataReq{
		Id: jobID,
		// Port: "80",
	}
	
	fmt.Println("start load ..............")
	res, err := client.ReadLersiData(context.Background(), req)
	fmt.Println("end load..............")
	if err != nil {
		//eturn fmt.Printf("err : %s\n", err)
	}
	// fmt.Println(res.LersiData)
	jsonString, _ := json.Marshal(res.LersiData)
	fmt.Println(string(jsonString))

	//fmt.Printf("Get : %d, statusCode: %s\n", req)
	return res.LersiData.Output
}

func GetDBClient(jobID string) {
	opts := []grpc.DialOption{grpc.WithInsecure()}
	conn, err := grpc.Dial("127.0.0.1:10000", opts...)
	if err != nil {
		panic(err)
	}

	client := pb.NewLersiDataServiceClient(conn)
	res := GetData(client, jobID)
	// if err != nil {
	// 	//panic(err)
	fmt.Println("res", res)
	// }
	fmt.Println("Finish ")
}

/// fortest
func main() {
	GetDBClient("8006d859-c127-4518-8a8b-d60e3e43e34a")
}
