# Dockerfile References: https://docs.docker.com/engine/reference/builder/


# Start from the latest golang base image
FROM golang:1.16.0-alpine3.13 AS builder
RUN apk add build-base libpcap-dev

USER root
# Add Maintainer Info
LABEL maintainer="popinlive@gmail.com"

# Set the Current Working Directory inside the container
WORKDIR /go/src/app/

# Copy go mod and sum files
COPY go.mod ./

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

# Build the Go app
RUN go build -o server .
CMD ["./server"]
